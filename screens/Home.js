import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

function Home() {
  return (
    <View style={styles.container}>
        <Text style={styles.heading}>Press enter to start</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            Actions.accounts();
        }}
      >
          <Text style={styles.btntext}>Enter</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    fontSize: 30,
    margin: 20,
  },
  button: {
    marginTop: 30,
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 35,
    paddingVertical: 25,
    paddingHorizontal: 30
  },
  btntext: {
    fontSize: 20,
    textTransform: 'uppercase',
    color: '#fff',
  }
});

export default Home;