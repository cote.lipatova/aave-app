import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, Picker } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Overlay, ListItem } from 'react-native-elements';
import PickerModal from 'react-native-picker-modal-view';


  const items = [
    {
        id:0,
        number: 'GB53BARC20038011756653',
        amount: '18500£',
    },
    {
        id:1,
        number: 'GB53BARC20038096855917',
        amount: '29700£',
      
    },
    {
        id:2,
        number: 'GB79BARC20040441934239',
        amount: '136200£',
    }
  ];

function Accounts() {
    const [visible, setVisible] = useState(false);
    const [selectedValue, setSelectedValue] = useState("gbc");

    const toggleOverlay = () => {
        setVisible(!visible);
    };

    const myRefs = React.useRef([]);
    const buildView = () => {
        return items.map(item =>{
          return(
            <TouchableOpacity
                onPress={() => {
                    Actions.operations();
                }}
            >
            <ListItem ref={el => myRefs.current[item.id] = el} bottomDivider>
                <ListItem.Title>{item.number}</ListItem.Title>
                <ListItem.Subtitle>{item.amount}</ListItem.Subtitle>
            </ListItem>
            </TouchableOpacity>
  
          )
        });
    }

    return (
        <View style={styles.container}>
            <Text style={styles.heading}>Your accounts</Text>
            {buildView()}
            <TouchableOpacity
                style={styles.button}
                onPress={toggleOverlay}
            >
                <Text style={styles.btntext}>+</Text>
            </TouchableOpacity>
            <Overlay style={styles.overlay} isVisible={visible} onBackdropPress={toggleOverlay}>
                <Text style={styles.overlaytext}>Create new account</Text>
                <View style={{ borderWidth: 1, borderColor: 'black', borderRadius: 4 }}>
                  <Picker
                    selectedValue={selectedValue}
                    style={{ height: 50, width: 200, alignSelf: 'center' }}
                    itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:17 }}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                  >
                    <Picker.Item label="GBC" value="gbc" />
                    <Picker.Item label="EUR" value="euro" />
                  </Picker>
                </View>
                <TouchableOpacity
                  style={styles.btn_modal}
                  onPress={() => {
                    Actions.accounts(); toggleOverlay();
                }}
              >
                  <Text style={styles.btntext}>Create</Text>
              </TouchableOpacity>
            </Overlay>
        </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  heading: {
    fontSize: 30,
    margin: 20,
  },
  button: {
    marginTop: 30,
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 35,
    paddingVertical: 25,
    paddingHorizontal: 30
  },
  btn_modal: {
    margin: 20,
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 35,
    paddingVertical: 10,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btntext: {
    fontSize: 20,
    textTransform: 'uppercase',
    color: '#fff',
  },
  overlay: {
      height: 100,
      width: 100
  },
  overlaytext: {
    margin: 20,
    fontSize: 20
}
});

export default Accounts;