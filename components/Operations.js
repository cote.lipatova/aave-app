import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Content, InputGroup, Input } from 'native-base';
import { Overlay } from 'react-native-elements';


function Operations() {

  const [visible, setVisible] = useState(false);
  const toggleOverlay = () => {
      setVisible(!visible);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Select an action</Text>
      <TouchableOpacity
          style={styles.actionbutton}
          onPress={toggleOverlay}
      >
          <Text style={styles.btntext}>Transfer money</Text>
      </TouchableOpacity>
      <TouchableOpacity
          style={styles.actionbutton}
          onPress={toggleOverlay}
      >
          <Text style={styles.btntext}>Make a payment</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.actionbutton}
        onPress={toggleOverlay}
      >
        <Text style={styles.btntext}>Create standing order</Text>
      </TouchableOpacity>
      <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
        <Text style={styles.heading}>Transfer money</Text>
        <Content>
          <InputGroup style={styles.acgroup}>
            <Input placeholder="From" style={styles.input}/>
            <Input placeholder="To"/>
            <Input placeholder="Amount £"/>
            </InputGroup>
          <TouchableOpacity
              style={styles.button}
              onPress={() => {
                Actions.accounts(); toggleOverlay();
            }}
          >
              <Text style={styles.btntext}>SEND</Text>
          </TouchableOpacity>
          <TouchableOpacity
              style={styles.resetbutton}
              onPress={toggleOverlay}
          >
              <Text style={styles.btntext}>RESET</Text>
          </TouchableOpacity>
        </Content>
      </Overlay>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  heading: {
    fontSize: 30,
    margin: 20,
  },
  acgroup: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 30,
    width: 300,
  },
  input: {
    height: 50,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  button: {
    elevation: 3,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resetbutton: {
    elevation: 3,
    backgroundColor: "#96000e",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionbutton: {
    elevation: 3,
    backgroundColor: "#dcdcdc",
    borderRadius: 10,
    height: 80,
    width: '90%',
    paddingVertical: 10,
    paddingHorizontal: 12,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btntext: {
    fontSize: 20,
    textTransform: 'uppercase',
    color: '#fff',
  }
});

export default Operations;