import 'react-native-gesture-handler';
import React from 'react';
import { Scene, Router, Stack } from 'react-native-router-flux';


import Home from './screens/Home';
import Accounts from './screens/Accounts';
import Operations from './components/Operations';

const screens = [
  { key: "home", component: Home, title: "home" },
  { key: "accounts", component: Accounts, title: "accounts" },
  { key: "operations", component: Operations, title: "operations" },
]

const initScreenKey = 'home'

export default App = () => (
  <Router>
    <Stack key="root">
      {
        screens.map(({key, title, component}) =>
          <Scene
            key={key} hideNavBar component={component}
            title={title} initial={key === initScreenKey}
          />
        )
      }
    </Stack>
  </Router>
);
